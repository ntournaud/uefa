
chrome.storage.sync.get(null, function(data) {
  // ***************************** CHECK IF USEFUL
  if (data.tracking || data.tracking === undefined) {
    var CATEGORY_EVENT = analytics.EventBuilder.builder().category('Category').action('Choose');
    var service, tracker;

    function startApp() {

      // Initialize the Analytics service object with the name of your app.
      service = analytics.getService('playad');
      service.getConfig().addCallback(initAnalyticsConfig);
      // Get a Tracker using your Google Analytics app Tracking ID.
      tracker = service.getTracker('UA-77506535-2');
      // 6.5.1 Record the analytics parameters
      var user_id = data.userid;
      var banner_id = 0;
      var publisher = 'popup';
      var action = 'opened';

      // 6.5.2 Date
      var d = new Date();
      var curr_date = d.getDate();
      var curr_month = d.getMonth() + 1; //Months are zero based
      var curr_year = d.getFullYear();
      var date = curr_year + '-' + curr_month + '-' + curr_date;

      // 6.5.3 Check with timestamp
      var ts = Math.floor(Date.now() / 1000)

      var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
      tracker.sendEvent(parameters, action, '', 1);
     
      // Track what happens when checkboxes are clicked
      var button1 = document.getElementById('news');
      var button2 = document.getElementById('social');
      var button3 = document.getElementById('local');
      [button1, button2, button3].forEach(addButtonListener);

      // setupAnalyticsListener();
    }

    function addButtonListener(button) {
      button.addEventListener('click', function() {
        // Another way of sending an Event (using the EventBuilder). This method gives you more control over the contents of the hit.
        // 6.5.1 Record the analytics parameters
        var user_id = data.userid;
        var banner_id = button.id;
        var publisher = 'popup';
        var action = 'click to '+ button.checked;

        // 6.5.2 Date
        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();
        var date = curr_year + '-' + curr_month + '-' + curr_date;

        // 6.5.3 Check with timestamp
        var ts = Math.floor(Date.now() / 1000)

        var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
        tracker.sendEvent(parameters, action, '', 1);
      });
    }

    window.onload = startApp;

    // ***************************** TO-DO PRIVACY & check privacy event

    function initAnalyticsConfig(config) {

      var checkbox = document.getElementById('tracking-permitted');
      config.setTrackingPermitted(checkbox.checked);
      checkbox.onchange = function() {
        config.setTrackingPermitted(checkbox.checked);
      };
    }
    /**
     * Adds a filter that captures hits being sent to Google Analytics.
     * Filters are useful for keeping track of what's happening in your app...
     * you can show this info in a debug panel, or log them to the console.
     */
    // function setupAnalyticsListener() {
    //   // Listen for event hits of the 'Flavor' category, and record them.
    //   previous = [];
    //   tracker.addFilter(
    //       analytics.filters.FilterBuilder.builder().
    //           whenHitType(analytics.HitTypes.EVENT).
    //           whenValue(analytics.Parameters.EVENT_CATEGORY, 'Category').
    //           whenValue(analytics.Parameters.EVENT_ACTION, 'Choose').
    //           applyFilter(
    //               function(hit) {
    //                 previous.push(
    //                     hit.getParameters().get(analytics.Parameters.EVENT_LABEL));
    //               }).
    //           build());
    // }


    // Start timing...
    // var timing = tracker.startTiming('Analytics Performance', 'Send Event');
    // Record an "appView" each time the user launches your app or goes to a new
    // screen within the app.
    // tracker.sendAppView('Popup');
    // Record user actions with "sendEvent".
    // ...send elapsed time since we started timing.
    // timing.send();
  }
});