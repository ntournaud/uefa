/*USER IDENTIFICATION*/
function getRandomToken() {
// E.g. 8 * 32 = 256 bits token
    var randomPool = new Uint8Array(32);
    crypto.getRandomValues(randomPool);
    var hex = '';
    for (var i = 0; i < randomPool.length; ++i) {
        hex += randomPool[i].toString(16);
    }
    // E.g. db18458e2782b2b77e36769c569e263a53885a9944dd0a861e5064eac16f1a
    return hex;
}

// REAL CODE STARTS HERE

$.getJSON("https://enigmatic-reaches-58089.herokuapp.com/live.json", function(json) {
    chrome.storage.sync.get(null, function(data) {
        // 1. SELECT a random item of the list among the categories checked in the popup
        var array = [];
        jQuery.each(json, function(i, val) {
            var blacklist = false;
            if (data.news == false) {
                if (val.category == "news") {blacklist = true;}
            }
            if (data.social == false) {
                if (val.category == "social") {blacklist = true;}
            }
            if (data.local == false) {
                if (val.category == "local") {blacklist = true;}
            }
            if (data.newsIds) {
                for (j = 0; j < data.newsIds.length; j++) {
                    if (data.newsIds[j].id == i) {
                        blacklist = true;
                    }
                }
            }
            if (val.id == 122 || data.previousId == i) {
                blacklist = true;
            }
            if (blacklist === false) {
                var diff = Math.floor((new Date() - new Date(val.created_at))/(1000*60*60*24));
                if (diff == 0) {
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                } else if (diff == 1) {
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                } else if (diff == 2) {
                    array.splice(0,0,i);
                    array.splice(0,0,i);
                } else {
                    array.splice(0,0,i);
                }
            }
        });
        var rand = array[Math.floor(Math.random() * array.length)];

        // Store the banner_id of the newly displayed
        chrome.storage.sync.set({previousId: rand}, function () {
            // you can use strings instead of objects
            // if you don't  want to define default values
            chrome.storage.sync.get('previousId', function (data) {
                console.log(data.newsIds)
            });
        });
        
        // 2. SELECT a random item of the selected category which meets the criteria of being live
        if (rand === undefined) {
            var select = json[122];
            console.log(select);
        } else {
            var select = json[rand];
        }

        // 3. SET the picture & video
        var picture = select.picture;
        if (select.category != 'social') {
            $('.header').css('background-image', "linear-gradient( rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25) ), url("+picture+")");
        } 
        else if (picture.match(/.mp4/)) {
            $('.insta-video').removeClass('hide');
            $('.insta-video source').attr('src',picture);
            $('.background-image').css('background-image',"url(https://s3-eu-west-1.amazonaws.com/vulcain/Pics/picturedefault.jpg)");
            $('.background-image').removeClass('hide');
            $('.insta-video')[0].load();
        } 
        else {
            $('.background-image').removeClass('hide');
            $('.insta-image').removeClass('hide');
            $('.background-image').css('background-image', "url("+picture+")");
            $('.insta-image').attr('src',picture);
        }

        // 4. SET the text
        var text1 = $('.text1');
        var text2 = $('.text2');
        var text3 = $('.text3');
        var link1 = $('.link1');
        var banner_id = $('#banner_id');
        var user_id = $('#user_id');

        var text1Content = select.text_title;
        var text2Content = select.text_subtitle;
        var ctaContent = select.text_cta;
        var link1Content = select.link1;

        // 5. COLLECT user_id
        var userid = data.userid;
        if (userid) {
        } else {
            userid = getRandomToken();
            chrome.storage.sync.set({userid: userid}, function() {
            });
        }

        text1.html(text1Content);
        text2.html(text2Content);
        link1.attr('href', link1Content);
        link1.html(ctaContent);
        banner_id.html(select.id);
        user_id.html(data.userid);


        // 6. ANALYTICS
        if (data.tracking || data.tracking === undefined) {
            var service, tracker;

            service = analytics.getService('playad');
            tracker = service.getTracker('UA-77506535-2');

            // 6.1 Record the analytics parameters
            var user_id = data.userid;
            var banner_id = select.id;
            var publisher = 'newtab';
            var action = 'impression';

            // 6.2 Date
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            var date = curr_year + '-' + curr_month + '-' + curr_date;

            // 6.3 Check with timestamp
            var ts = Math.floor(Date.now() / 1000)

            var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
            console.log(parameters);
            
            // 6.4 Analytics' logic 
            tracker.sendAppView('New tab');
            tracker.sendEvent(parameters, action, '', 1);
            
            // 6.5 Track clicks on goGoGO material
            var button1 = $('.link1');
            addButtonListener(button1[0]);

            function addButtonListener(button) {
                button.addEventListener('click', function() {

                // 6.5.1 Record the analytics parameters
                var user_id = data.userid;
                var banner_id = select.id;
                var publisher = 'newtab';
                var action = 'click';

                // 6.5.2 Date
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth() + 1; //Months are zero based
                var curr_year = d.getFullYear();
                var date = curr_year + '-' + curr_month + '-' + curr_date;

                // 6.5.3 Check with timestamp
                var ts = Math.floor(Date.now() / 1000)

                var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
                tracker.sendEvent(parameters, action, '', 1);
                
              });
            }
            // 6.6 Track clicks on video material
            $('.insta-video').bind('play', function (e) {
                // 6.5.1 Record the analytics parameters
                var user_id = data.userid;
                var banner_id = select.id;
                var publisher = 'newtab';
                var action = 'play';

                // 6.5.2 Date
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth() + 1; //Months are zero based
                var curr_year = d.getFullYear();
                var date = curr_year + '-' + curr_month + '-' + curr_date;

                // 6.5.3 Check with timestamp
                var ts = Math.floor(Date.now() / 1000)

                var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
                tracker.sendEvent(parameters, action, '', 1);
            });

        }
        
    })
});

// Storing banner_id when clicked

function registerId() {
    var id = document.getElementById('banner_id').innerHTML;
    var newid = true;
    chrome.storage.sync.get({newsIds: []}, function (data) {
        // the input argument is ALWAYS an object containing the queried keys
        // so we select the key we need
        var newsIds = data.newsIds;
        console.log("here");
        for (i = 0; i < newsIds.length; i++) {
            if (id == newsIds[i].id) {
                newid = false;
            }
        }  
        if (newid) {
            newsIds.push({id: id});
        }
        // set the new array value to the same key
        chrome.storage.sync.set({newsIds: newsIds}, function () {
            // you can use strings instead of objects
            // if you don't  want to define default values
            chrome.storage.sync.get('newsIds', function (data) {
                console.log(data.newsIds)
            });
        });
    });
}

$(".link1").click(registerId);
$(".logo-2016").click(registerId);
$('.insta-video').bind('play', registerId);