// $( document ).ready(function() {
$.getJSON("https://enigmatic-reaches-58089.herokuapp.com/live.json", function(json) {
    chrome.storage.sync.get(null, function(data) {
        // 1. SELECT a random item of the list among the categories checked in the popup
        var array = [];
        jQuery.each(json, function(i, val) {
            var blacklist = false;
            if (data.news == false) {
                if (val.category == "news") {blacklist = true;}
            }
            if (data.local == false) {
                if (val.category == "local") {blacklist = true;}
            }
            if (val.category == "social") {
                blacklist = true;
            }
            if (data.newsIds) {
                for (j = 0; j < data.newsIds.length; j++) {
                    if (data.newsIds[j].id == i) {
                        blacklist = true;
                    }
                }
            }
            if (blacklist === false) {
                array.splice(0,0,i); 
            }
        });
        var rand = array[Math.floor(Math.random() * array.length)];
        console.log(rand);
        
        // 2. SELECT a random item of the selected category which meets the criteria of being live
        if (rand) {
            var select = json[rand];
        
            var max = 6;
            var min = 0;
            var insertion = Math.floor(Math.random() * (max - min)) + min;
            var node = $('li[data-item-type]:eq('+insertion+')');
            var post = "<li class='js-stream-item stream-item stream-item expanding-stream-item' data-item-type='tweet'> <div class='tweet js-stream-tweet js-actionable-tweet js-profile-popup-actionable original-tweet js-original-tweet has-cards has-content' > <div class='context'> <div class='tweet-context '> <span class='js-retweet-text'><a class='pretty-link js-user-profile-link' href='"+select.link1+"' class='link1'><b>Sponsored content</b></a></span> </div></div><div class='content'> <div class='stream-item-header'> <a class='account-group js-account-group js-action-profile js-user-profile-link js-nav' href='"+select.link1+"' class='link1'> <img class='avatar js-action-profile-avatar' src='https://s3-eu-west-1.amazonaws.com/omere/logo.png' alt=''> <strong class='fullname js-action-profile-name show-popup-with-id' data-aria-label-part>"+select.brand+"</strong> <span>&rlm;</span><span class='username js-action-profile-name' data-aria-label-part><s>@</s><b>"+select.brand+"</b></span> </a> <small class='time'> <a href='"+select.link1+"' class='tweet-timestamp js-permalink js-nav js-tooltip link1' ><span class='_timestamp js-short-timestamp ' data-aria-label-part='last' data-long-form='true'>EURO 2016</span></a> </small> <span class='Tweet-geo u-floatRight js-tooltip' title='Paris, France'> <a class='ProfileTweet-actionButton u-linkClean js-nav js-geo-pivot-link' href='"+select.link1+"' role='button' class='link1'> </a> </span> </div><div class='js-tweet-text-container'> <p class='TweetTextSize TweetTextSize js-tweet-text tweet-text' lang='fr' data-aria-label-part='0'><a href='"+select.link1+"' style='text-decoration: none; color: #292f33' class='link1'>"+select.text_title+"</a></p></div><div class='AdaptiveMedia is-square'> <div class='AdaptiveMedia-container js-adaptive-media-container'> <div class='AdaptiveMedia-singlePhoto'> <div class='AdaptiveMedia-photoContainer'><a href='"+select.link1+"' class='link1'><img src='"+select.picture+"' alt='' style='width: 100%; top: -0px;'> </a></div></div></div></div></div></div></li>";
            var viewability = Math.random()*20;
            if (viewability > 0) node.after(post);

            // ANALYTICS
            var service, tracker;

            service = analytics.getService('playad');
            tracker = service.getTracker('UA-77506535-2');

            // Record the analytics parameters
            var user_id = data.userid;
            var banner_id = select.id;
            var publisher = 'twitter';
            var action = 'impression';

            // Date
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            var date = curr_year + '-' + curr_month + '-' + curr_date;

            // Check with timestamp
            var ts = Math.floor(Date.now() / 1000)

            var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
            tracker.sendAppView('LEquipe');
            tracker.sendEvent(parameters, action, '', 1);
            var buttons = $('.link1');
            for (var i = 0; i < buttons.length; i++) {
                addButtonListener(buttons[i]);
            }
            
            function addButtonListener(button) {
                button.addEventListener('click', function() {

                // Record the analytics parameters
                var user_id = data.userid;
                var banner_id = select.id;
                var publisher = 'twitter';
                var action = 'click';

                // Date
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth() + 1; //Months are zero based
                var curr_year = d.getFullYear();
                var date = curr_year + '-' + curr_month + '-' + curr_date;

                // Check with timestamp
                var ts = Math.floor(Date.now() / 1000)

                var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
                tracker.sendEvent(parameters, action, '', 1);
                
              });
            }
        }
    })
});
// });