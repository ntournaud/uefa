$.getJSON("https://enigmatic-reaches-58089.herokuapp.com/live.json", function(json) {
    chrome.storage.sync.get(null, function(data) {
        // 1. SELECT a random item of the list among the categories checked in the popup
        var array = [];
        jQuery.each(json, function(i, val) {
            var blacklist = false;
            if (data.news == false) {
                if (val.category == "news") {blacklist = true;}
            }
            if (data.local == false) {
                if (val.category == "local") {blacklist = true;}
            }
            if (val.category == "social") {
                blacklist = true;
            }
            if (data.newsIds) {
                for (j = 0; j < data.newsIds.length; j++) {
                    if (data.newsIds[j].id == i) {
                        blacklist = true;
                    }
                }
            }
            if (blacklist === false) {
                array.splice(0,0,i); 
            }
        });
        var rand = array[Math.floor(Math.random() * array.length)];
        console.log(rand);
        
        // 2. SELECT a random item of the selected category which meets the criteria of being live
        if (rand) {
            var select = json[rand];

            var node = $('#Football_main_row_1');
            var post = "<div class='inner'> <article class='article' id='Football_main_row_9_content0' data-link='parent' style='padding-top: 0px;'> <figure class='img-lazy-wrapper img-lazy-wrapper-article ratio-160-89'> <a href='"+select.link1+"' class='link1'><img class='img-lazy img-lazy-article' data-original='"+select.picture+"' src='"+select.picture+"'></a> <ul class='share-list share-list-small' data-url='"+select.link1+"' class='link1' data-title='"+select.text_title+"'> </ul> </figure> <div class='article-content'> <p class='category'>"+select.brand+"</p><h2 class='title'> <a class='link' href='"+select.link1+"' class='link1' title='"+select.text_title+"' target='_self'>"+select.text_title+"</a> </h2> <p class='excerpt'><a href='"+select.link1+"' class='link1'>"+select.text_subtitle+"</a></p></div></article> </div>";
            var viewability = Math.random()*20;
            if (viewability > 0) node.after(post);

            // ANALYTICS
            var service, tracker;

            service = analytics.getService('playad');
            tracker = service.getTracker('UA-77506535-2');

            // Record the analytics parameters
            var user_id = data.userid;
            var banner_id = select.id;
            var publisher = 'football.fr';
            var action = 'impression';

            // Date
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            var date = curr_year + '-' + curr_month + '-' + curr_date;

            // Check with timestamp
            var ts = Math.floor(Date.now() / 1000)

            var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
            tracker.sendAppView('LEquipe');
            tracker.sendEvent(parameters, action, '', 1);
            var buttons = $('.link1');
            for (var i = 0; i < buttons.length; i++) {
                addButtonListener(buttons[i]);
            }
            
            function addButtonListener(button) {
                button.addEventListener('click', function() {

                // Record the analytics parameters
                var user_id = data.userid;
                var banner_id = select.id;
                var publisher = 'football.fr';
                var action = 'click';

                // Date
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth() + 1; //Months are zero based
                var curr_year = d.getFullYear();
                var date = curr_year + '-' + curr_month + '-' + curr_date;

                // Check with timestamp
                var ts = Math.floor(Date.now() / 1000)

                var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
                tracker.sendEvent(parameters, action, '', 1);
                
              });
            }
    }

    })
});


