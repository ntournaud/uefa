$.getJSON("https://enigmatic-reaches-58089.herokuapp.com/live.json", function(json) {
    chrome.storage.sync.get(null, function(data) {
        // 1. SELECT a random item of the list among the categories checked in the popup
        var array = [];
        jQuery.each(json, function(i, val) {
            var blacklist = false;
            if (data.news == false) {
                if (val.category == "news") {blacklist = true;}
            }
            if (data.local == false) {
                if (val.category == "local") {blacklist = true;}
            }
            if (val.category == "social") {
                blacklist = true;
            }
            if (data.newsIds) {
                for (j = 0; j < data.newsIds.length; j++) {
                    if (data.newsIds[j].id == i) {
                        blacklist = true;
                    }
                }
            }
            if (blacklist === false) {
                array.splice(0,0,i); 
            }
        });
        var rand = array[Math.floor(Math.random() * array.length)];
            
        // 2. SELECT a random item of the selected category which meets the criteria of being live
        if (rand) {
            var select = json[rand];

            // select the place of insertion of the ad 
    		var max = 0;
    		var min = 0;
    		var insertion = Math.floor(Math.random() * (max - min)) + min;
    		var node = $('.BigImage:eq('+insertion+1+')');

    		// select the content to insert
    		var post = "<div class='Aussi WithImage Coleader BigImage clearfix' rub='Aussi' sport='4' type='EDITOSPORT' data-vr-contentbox=''><div class='ZoneClick' data-prog-infos='' data-prog-active='' data-abonne='0' data-bandeau_match='' data-bandeau_live_e21='' data-native_ads=''><div><div class='les-blocs'><span class='lespor-t'>"+select.brand+"</span> <span class='letitr-e' json='surtitre[].texte'>SPONSORED</span></div><a href='"+select.link1+"' class='link1'><img class='lazy' src='"+select.picture+"' width='300' height='218' taille='1' style='display: inline;'></a><div class='bandeau_live_e21'></div></div><div class='bandeau_live_e21'></div><div class='ZoneEdito'><h2><a href='"+select.link1+"' infog='false' onglet='' class='link1'>"+select.text_title+"</a></h2><p><a href='"+select.link1+"' class='link1'>"+select.text_subtitle+"</a></p></div></div><ul class='UnePlus'><li><a href='"+select.link1+"' json='lien[].href;lien[].texte;lien[].onglet' onglet='0' class='link1'>"+select.text_cta+"</a></li></ul></div>";

    		// select the frequency of appearance of the ads
            var viewability = Math.random()*20;
        	if (viewability > 0) node.after(post);

            // ANALYTICS
            var service, tracker;

            service = analytics.getService('playad');
            tracker = service.getTracker('UA-77506535-2');

            // Record the analytics parameters
            var user_id = data.userid;
            var banner_id = select.id;
            var publisher = 'lequipe';
            var action = 'impression';

            // Date
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            var date = curr_year + '-' + curr_month + '-' + curr_date;

            // Check with timestamp
            var ts = Math.floor(Date.now() / 1000)

            var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
            tracker.sendAppView('LEquipe');
            tracker.sendEvent(parameters, action, '', 1);
            var buttons = $('.link1');
            for (var i = 0; i < buttons.length; i++) {
                addButtonListener(buttons[i]);
            }

            function addButtonListener(button) {
                button.addEventListener('click', function() {

                // Record the analytics parameters
                var user_id = data.userid;
                var banner_id = select.id;
                var publisher = 'LEquipe';
                var action = 'click';

                // Date
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth() + 1; //Months are zero based
                var curr_year = d.getFullYear();
                var date = curr_year + '-' + curr_month + '-' + curr_date;

                // Check with timestamp
                var ts = Math.floor(Date.now() / 1000)

                var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
                tracker.sendEvent(parameters, action, '', 1);
                
              });
            }
        }
    });
});
