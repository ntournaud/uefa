$.getJSON("https://enigmatic-reaches-58089.herokuapp.com/live.json", function(json) {
    chrome.storage.sync.get(null, function(data) {
        // 1. SELECT a random item of the list among the categories checked in the popup
        var array = [];
        jQuery.each(json, function(i, val) {
            var blacklist = false;
            if (data.news == false) {
                if (val.category == "news") {blacklist = true;}
            }
            if (data.local == false) {
                if (val.category == "local") {blacklist = true;}
            }
            if (val.category == "social") {
                blacklist = true;
            }
            if (data.newsIds) {
                for (j = 0; j < data.newsIds.length; j++) {
                    if (data.newsIds[j].id == i) {
                        blacklist = true;
                    }
                }
            }
            if (blacklist === false) {
                array.splice(0,0,i); 
            }
        });
        var rand = array[Math.floor(Math.random() * array.length)];
        console.log(rand);
        
        // 2. SELECT a random item of the selected category which meets the criteria of being live
        if (rand) {
            var select = json[rand];
        
            var max = 2;
            var min = 0;
            var insertion = Math.floor(Math.random() * (max - min)) + min;
            
            if ($('#otherselector').length ) {
                var node = $('#substream_0');
                var post = "<li> <div class='mvs _5v9_ _5jmm _5v3q _5pat _5va1 _3ccb'> <div></div><div class='userContentWrapper _5pcr'> <div class='_1dwg _1w_m'> <div class='_4r_y'> <div class='_6a uiPopover _5pbi _cmw _5v56 _b1e'></div></div><div class='_4gns accessible_elem'></div><div class='_5x46'> <div class='clearfix _5va3'> <a class='_5pb8 _8o _8s lfloat _ohe' href='"+select.link1+"' aria-hidden='true' tabindex='-1' target=''> <div class='_38vo'><img class='_s0 _5xib _5sq7 _44ma _rw img' src='https://s3-eu-west-1.amazonaws.com/omere/logo.png' alt=''></div></a> <div class='clearfix _42ef'> <div class='rfloat _ohf'></div><div class='_5va4'> <div> <div class='_6a _5u5j'> <div class='_6a _6b' style='height:40px'></div><div class='_6a _5u5j _6b'> <h6 class='_5pbw'><span class='fwn fcg'><span class='fwb fcg'><a href='"+select.link1+"'>"+select.brand+"</a></span></span></h6> <div class='_5pcp'><span><span class='fsm fwn fcg'><a class='_5pcq' href='"+select.link1+"' target=''><abbr data-shorten='1' class='_5ptz timestamp livetimestamp'><span class='timestampContent'>Sponsored content</span></abbr></a></span></span><span role='presentation' aria-hidden='true'> · </span></div></div></div></div></div></div></div></div><div class='_5pbx userContent' id='js_4'> <p>"+select.text_title+"</p><div class='_5wpt'></div></div><div class='_3x-2'> <div> <div class='mtm'> <div id='u_ps_0_0_i' class='_6m2 _1zpr clearfix _dcs _4_w4 _59ap'> <div class='clearfix _2r3x'> <div class='lfloat _ohe'> <span class='_3m6-'> <div class='_6ks'> <a href='"+select.link1+"' tabindex='-1' target='_blank' rel='nofollow'> <div class='_6l- __c_'> <div class='uiScaledImageContainer _6m5 fbStoryAttachmentImage' style='width:450px;height:236px;'><img class='scaledImageFitWidth img' src='"+select.picture+"' alt='' width='450' height='236'></div></div></a> </div><div class='_3ekx _29_4'> <div class='_6m3'> <div class='mbs _6m6 _2cnj _5s6c'><a href='"+select.link1+"' rel='nofollow' target='_blank'>"+select.text_title+"</a></div><div class='_6m7 _3bt9'>"+select.text_subtitle+"</div><div class='_59tj'> <div> <div class='_6lz _6mb ellipsis'>"+select.brand+"</div><div class='_5tc6'></div></div></div></div><a class='_52c6' href='"+select.link1+"' tabindex='-1' target='_blank' rel='nofollow'></a> </div></span> </div><div class='_42ef'><span class='_3c21'></span></div></div></div></div></div></div></div><div> </div></div></div></li>";
                var viewability = Math.random()*20;
                if (viewability > 0) node.after(post);
            }
            else if ($('#substream_0').length ) {
                var node = $('#substream_0');
                var post = "<div class='_4ikz' id='substream_0' data-referrer='substream_0'> <div> <div id='u_ps_0_0_0'> <div> <div class='_5jmm _5pat _3lb4 _x72'> <div class='_4-u2 mbm _5v3q _4-u8'> <div class='_3ccb'> <div></div><div class='userContentWrapper _5pcr' role='article' aria-label='Actualité'> <div class='_1dwg _1w_m'> <div class='_4r_y'> <div class='_6a uiPopover _5pbi _cmw _5v56 _b1e'><a class='_4xev _p' href='#' rel='toggle' role='button' id='u_ps_0_0_4'></a></div></div><div class='_4gns accessible_elem' id='js_2'></div><div class='_5x46'> <div class='clearfix _5va3'> <a class='_5pb8 _8o _8s lfloat _ohe link1' href='"+select.link1+"' aria-hidden='true' tabindex='-1' target=''> <div class='_38vo'><img class='_s0 _5xib _5sq7 _44ma _rw img' src='https://s3-eu-west-1.amazonaws.com/omere/logo.png' alt=''></div></a> <div class='clearfix _42ef'> <div class='rfloat _ohf'></div><div class='_5va4'> <div> <div class='_6a _5u5j'> <div class='_6a _6b' style='height:40px'></div><div class='_6a _5u5j _6b'> <h5 class='_5pbw' id='js_3'><span class='fwn fcg'><span class='fwb fcg'><a href='"+select.link1+"' class='link1'>"+select.brand+"</a></span></span></h5> <div class='_5pcp'><span><span class='fsm fwn fcg'><a class='_5pcq link1' href='"+select.link1+"' target=''><abbr data-shorten='1' class='_5ptz timestamp livetimestamp'><span class='timestampContent' id='js_4'>Sponsored content</span></abbr></a></span></span><span role='presentation' aria-hidden='true'> · </span></div></div></div></div></div></div></div></div><div class='_5pbx userContent' data-ft='{&quot;tn&quot;:&quot;K&quot;}' id='js_5'> <p><a href='"+select.link1+"' style='text-decoration: none; color: #1d2129;' class='link1'>"+select.text_title+"</a></p><div class='_5wpt'></div></div><div class='_3x-2'> <div> <div class='mtm'> <div id='u_ps_0_0_5' class='_6m2 _1zpr clearfix _dcs _4_w4 _59ap'> <div class='clearfix _2r3x'> <div class='lfloat _ohe'> <span class='_3m6-'> <div class='_6ks'> <a href='"+select.link1+"' tabindex='-1' target='_blank' rel='nofollow' class='link1'> <div class='_6l- __c_'> <div class='uiScaledImageContainer _6m5 fbStoryAttachmentImage' style='width:476px;height:249px;'><img class='scaledImageFitWidth img' src='"+select.picture+"' alt='' width='476' height='249'></div></div></a> </div><div class='_3ekx _29_4'> <div class='_6m3'> <div class='mbs _6m6 _2cnj _5s6c'><a href='"+select.link1+"' class='link1' rel='nofollow' target='_blank'>"+select.text_title+"</a></div><div class='_6m7 _3bt9'>"+select.text_subtitle+"</div><div class='_59tj'> <div> <div class='_6lz _6mb ellipsis'>"+select.brand+"</div><div class='_5tc6'></div></div></div></div><a class='_52c6 link1' href='"+select.link1+"'></a> </div></span> </div><div class='_42ef'><span class='_3c21'></span></div></div></div></div></div></div></div></div></div></div><div></div></div></div></div></div></div>";
                var viewability = Math.random()*20;
                if (viewability > 0) node.after(post);
            }
            else {
                console.log("No selector found, nothing to push");
            }
            // ANALYTICS
            var service, tracker;

            service = analytics.getService('playad');
            tracker = service.getTracker('UA-77506535-2');

            // Record the analytics parameters
            var user_id = data.userid;
            var banner_id = select.id;
            var publisher = 'facebook';
            var action = 'impression';

            // Date
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            var date = curr_year + '-' + curr_month + '-' + curr_date;

            // Check with timestamp
            var ts = Math.floor(Date.now() / 1000)

            var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
            tracker.sendAppView('facebook');
            tracker.sendEvent(parameters, action, '', 1);
            var buttons = $('.link1');
            for (var i = 0; i < buttons.length; i++) {
                addButtonListener(buttons[i]);
            }
            
            function addButtonListener(button) {
                button.addEventListener('click', function() {

                // Record the analytics parameters
                var user_id = data.userid;
                var banner_id = select.id;
                var publisher = 'LEquipe';
                var action = 'click';

                // Date
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth() + 1; //Months are zero based
                var curr_year = d.getFullYear();
                var date = curr_year + '-' + curr_month + '-' + curr_date;

                // Check with timestamp
                var ts = Math.floor(Date.now() / 1000)

                var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
                tracker.sendEvent(parameters, action, '', 1);
                
              });
            }
        }
    })
});