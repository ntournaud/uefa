// ANALYTICS
chrome.storage.sync.get("tracking", function(data) {
    if(data.tracking || data.tracking === undefined) {
        document.getElementById('tracking-permitted').checked = true;
        $('#tracking-permitted').siblings('label').html("Désactiver l'envoi de données anonymes");
    } else {
        document.getElementById('tracking-permitted').checked = false;
        $('#tracking-permitted').siblings('label').html("Réactiver l'envoi de données anonymes");
    }
})
$("#tracking-permitted").click(function() {
  chrome.storage.sync.get(null, function(data) {
    var tracking = document.getElementById('tracking-permitted').checked;
    var service, tracker;

    service = analytics.getService('playad');
    tracker = service.getTracker('UA-77506535-2');

    // 6.1 Record the analytics parameters
    var user_id = data.userid;
    var banner_id = 0;
    var publisher = 'popup';
    // 6.2 Date
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();
    var date = curr_year + '-' + curr_month + '-' + curr_date;
    // 6.3 Check with timestamp
    var ts = Math.floor(Date.now() / 1000)

    if (tracking) {
      var action = 'optin';
      var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;      
      tracker.sendEvent(parameters, action, '', 1);
      $('#tracking-permitted').siblings('label').html("Désactiver l'envoi de données anonymes");
    } else { 
      var action = 'optout';
      var parameters = user_id +';'+ date +';'+ ts +';'+ banner_id +';'+ publisher +';'+ action;
      tracker.sendEvent(parameters, action, '', 1);
      $('#tracking-permitted').siblings('label').html("Réactiver l'envoi de données anonymes");
    }
    chrome.storage.sync.set({'tracking': tracking}, function() {
        var status = document.getElementById('status');
        status.textContent = 'Your choice is recorded.';
        setTimeout(function() {
          status.textContent = '';
        }, 750);
    });
    chrome.storage.onChanged.addListener(function(changes, namespace) {
        for (key in changes) {
          var storageChange = changes[key];
          console.log('Storage key "%s" in namespace "%s" changed. ' +
                      'Old value was "%s", new value is "%s".',
                      key,
                      namespace,
                      storageChange.oldValue,
                      storageChange.newValue);
        }
    });
  });
});
// ALL CATEGORIES

// 1. NEWS
chrome.storage.sync.get("news", function(data) {
    if(data.news || data.news === undefined) {
        document.getElementById('news').checked = true;
    } else {
        document.getElementById('news').checked = false;
    }
})

$("#news").click(function() {
    var news = document.getElementById('news').checked;
    chrome.storage.sync.set({'news': news}, function() {
        var status = document.getElementById('status');
        status.textContent = 'Awesome choice !';
        setTimeout(function() {
          status.textContent = '';
        }, 750);
    });
    chrome.storage.onChanged.addListener(function(changes, namespace) {
        for (key in changes) {
          var storageChange = changes[key];
          console.log('Storage key "%s" in namespace "%s" changed. ' +
                      'Old value was "%s", new value is "%s".',
                      key,
                      namespace,
                      storageChange.oldValue,
                      storageChange.newValue);
        }
    });
});

// 2. SOCIAL
chrome.storage.sync.get("social", function(data) {
    if(data.social || data.social === undefined) {
        document.getElementById('social').checked = true;
    } else {
        document.getElementById('social').checked = false;
    }
})

$("#social").click(function() {
    var social = document.getElementById('social').checked;
    chrome.storage.sync.set({'social': social}, function() {
        var status = document.getElementById('status');
        status.textContent = 'Awesome choice !';
        setTimeout(function() {
          status.textContent = '';
        }, 750);
    });
    chrome.storage.onChanged.addListener(function(changes, namespace) {
        for (key in changes) {
          var storageChange = changes[key];
          console.log('Storage key "%s" in namespace "%s" changed. ' +
                      'Old value was "%s", new value is "%s".',
                      key,
                      namespace,
                      storageChange.oldValue,
                      storageChange.newValue);
        }
    });
});

// 3. LOCAL
chrome.storage.sync.get("local", function(data) {
    if(data.local || data.local === undefined) {
        document.getElementById('local').checked = true;
    } else {
        document.getElementById('local').checked = false;
    }
})

$("#local").click(function() {
    var local = document.getElementById('local').checked;
    chrome.storage.sync.set({'local': local}, function() {
        var status = document.getElementById('status');
        status.textContent = 'Awesome choice !';
        setTimeout(function() {
          status.textContent = '';
        }, 750);
    });
    chrome.storage.onChanged.addListener(function(changes, namespace) {
        for (key in changes) {
          var storageChange = changes[key];
          console.log('Storage key "%s" in namespace "%s" changed. ' +
                      'Old value was "%s", new value is "%s".',
                      key,
                      namespace,
                      storageChange.oldValue,
                      storageChange.newValue);
        }
    });
});

// ***************************************************************************************

/*USER IDENTIFICATION*/
function getRandomToken() {
// E.g. 8 * 32 = 256 bits token
    var randomPool = new Uint8Array(32);
    crypto.getRandomValues(randomPool);
    var hex = '';
    for (var i = 0; i < randomPool.length; ++i) {
        hex += randomPool[i].toString(16);
    }
    // E.g. db18458e2782b2b77e36769c569e263a53885a9944dd0a861e5064eac16f1a
    return hex;
}

chrome.storage.sync.get('userid', function(items) {
    var userid = items.userid;
    if (userid) {
        useToken(userid);
    } else {
        userid = getRandomToken();
        chrome.storage.sync.set({userid: userid}, function() {
            useToken(userid);
        });
    }
    function useToken(userid) {
        // TODO: Use user id for authentication or whatever you want.
    }
});